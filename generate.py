import argparse
from enum import Enum
import json
from os import path

from schedule import create_calendar
from zones.bike import BikeZones
from zwift import zwo


class Operation(Enum):
	ZWIFT = 'zwift'
	CALENDAR = 'calendar'

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('operation', type=Operation, choices=(Operation.CALENDAR, Operation.ZWIFT))
	parser.add_argument('--ftp-file', type=argparse.FileType('r'))
	parser.add_argument('--workouts-file', type=argparse.FileType('r'), required=True)
	parser.add_argument('--schedule-file', type=argparse.FileType('r'))
	args = parser.parse_args()


	if args.operation == Operation.ZWIFT:
		if not args.ftp_file:
			raise Exception('Generating zwift files requires ftp data')
		ftp_data = json.load(args.ftp_file)
		bike_zones = BikeZones(ftp_data['bike'])
		for line in args.workouts_file:
			workout = json.loads(line)
			if workout['type'] != 'bike':
				continue
			out_file = path.join('zwo_files', f"{workout['id']}.zwo")
			with open(out_file, 'w') as handler:
				zwo.generate_file(workout['id'], workout['workout'], bike_zones, handler)
	elif args.operation == Operation.CALENDAR:
		with open('workouts.ics', 'w') as handler:
			create_calendar.create_calendar(args.schedule_file, args.workouts_file, handler)

if __name__ == '__main__':
	main()