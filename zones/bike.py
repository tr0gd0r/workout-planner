from collections import namedtuple
import json

Range = namedtuple('Range', ['min', 'max'])

class BikeZones:
	RANGES = {
		1: Range(.5, .7),
		2: Range(.7, .83),
		3: Range(.91, 1),
		4: Range(1.02, 1.1),
		5: Range(1.1, 1.2)
	}

	def __init__(self, ftp):
		self.ftp = ftp

	@staticmethod
	def get_zone_range(zone):
		return BikeZones.RANGES[zone]

	def get_ftp_range(zone):
		min = self.ftp * float(RANGES[zone].min)
		max = self.ftp * float(RANGES[zone].max)
		return Range(min, max)
