from dataclasses import dataclass
from enum import Enum
import json
import re

import zwift.workouts

WORKOUTS_FILE = 'workouts.json'
ZONES_FILE = 'zones.json'
WORKOUTS_DIR = 'zwift_files'

class Tokens(Enum):
	NUMBER = 2
	LETTER = 3
	INTERVAL_START = 4
	INTERVAL_END = 5
	INTERVAL_SEPARATOR = 6


@dataclass
class Token:
	type: Tokens
	value: str


class RawStep:
	def __init__(self, step_string):
		self.step_string = step_string
		self.pos = 0

	def advance(self):
		self.pos += 1

	def current(self):
		return self.step_string[self.pos]

	def peek(self):
		try:
			return self.step_string[self.pos+1]
		except IndexError:
			return None

	def done(self):
		return self.pos >= len(self.step_string)


def is_number(n):
	try:
		int(n)
		return True
	except:
		return False


def is_letter(x):
	return bool(re.search(r'[A-Za-z]', x))


def lex(raw_step):
	step_tokens = []
	while True:
		if raw_step.done():
			break

		c = raw_step.current()

		if is_number(c):
			t = Tokens.NUMBER
			v = c
			while True:
				if is_number(raw_step.peek()):
					raw_step.advance()
					v += raw_step.current()
				else:
					break
			token = Token(t, v)
			step_tokens.append(token)
			raw_step.advance()

		elif is_letter(c):
			token = Token(Tokens.LETTER, c)
			step_tokens.append(token)
			raw_step.advance()

		elif c == '(':
			token = Token(Tokens.INTERVAL_START, '')
			step_tokens.append(token)
			raw_step.advance()

		elif c == ')':
			token = Token(Tokens.INTERVAL_END, '')
			step_tokens.append(token)
			raw_step.advance()

		elif c == '/':
			token = Token(Tokens.INTERVAL_SEPARATOR, '')
			step_tokens.append(token)
			raw_step.advance()

		elif c.isspace():
			raw_step.advance()
		else:
			raise Exception(f'Unrecognized character: {c}')

	return step_tokens


def parse(tokens, zones, is_first=False, is_last=False):
	pos = 0

	first_token = tokens[pos]
	# always starts with a number
	if first_token.type != Tokens.NUMBER:
		raise Exception(f'First token should be a number: {tokens}')

	pos += 1

	# next we find out if it's either an interval or steady state
	t = tokens[pos]
	if t.type == Tokens.LETTER and t.value == 'x':
		# interval :(
		reps = int(first_token.value)

		pos += 1

		if tokens[pos].type != Tokens.INTERVAL_START:
			raise Exception(f'Expected interval start token but got: {tokens[pos]}')

		pos += 1

		# should be interval on time token
		if tokens[pos].type != Tokens.NUMBER:
			raise Exception(f'Expected number token for on time but got: {tokens[pos]}')

		on_time = float(tokens[pos].value)

		pos += 1

		if not (tokens[pos].type == Tokens.LETTER and tokens[pos].value in ('m', 's')):
			raise Exception(f'Expected "m" for minutes or "s" for seconds but got: {tokens[pos]}')

		if tokens[pos].value == 'm':
			# convert to seconds
			on_time = int(on_time * 60)

		else:
			on_time = int(on_time)

		pos += 1

		if not (tokens[pos].type == Tokens.LETTER and tokens[pos].value.lower() == 'z'):
			raise Exception(f'Expected "Z" but got: {tokens[pos]}')

		pos += 1

		if not tokens[pos].type == Tokens.NUMBER:
			raise Exception(f'Expected zone number but got: {tokens[pos]}')

		on_zone = int(tokens[pos].value)
		on_power = zones.get_zone_range(on_zone).max

		pos += 1

		if tokens[pos].type != Tokens.INTERVAL_SEPARATOR:
			raise Exception(f'Expected interval separator token but got: {tokens[pos]}')

		pos += 1


		# should be interval off time token
		if tokens[pos].type != Tokens.NUMBER:
			raise Exception(f'Expected number token for off time but got: {tokens[pos]}')

		off_time = float(tokens[pos].value)

		pos += 1

		if not (tokens[pos].type == Tokens.LETTER and tokens[pos].value in ('m', 's')):
			raise Exception(f'Expected "m" for minutes or "s" for seconds but got: {tokens[pos]}')

		if tokens[pos].value == 'm':
			# convert to seconds
			off_time = int(off_time * 60)

		else:
			off_time = int(off_time)

		pos += 1

		if not (tokens[pos].type == Tokens.LETTER and tokens[pos].value.lower() == 'z'):
			raise Exception(f'Expected "Z" but got: {tokens[pos]}')

		pos += 1

		if not tokens[pos].type == Tokens.NUMBER:
			raise Exception(f'Expected zone number but got: {tokens[pos]}')

		off_zone = int(tokens[pos].value)
		off_power = zones.get_zone_range(off_zone).min

		pos += 1

		if tokens[pos].type != Tokens.INTERVAL_END:
			raise Exception(f'Expected interval end token but got: {tokens[pos]}')

		return zwift.workouts.Interval(reps, on_time, on_power, off_time, off_power)
	elif t.type == Tokens.LETTER and t.value == 'm': 
		# steady
		time = int(first_token.value) * 60

		pos += 1

		if not (tokens[pos].type == Tokens.LETTER and tokens[pos].value.lower() == 'z'):
			raise Exception(f'Expected "Z" but got: {tokens[pos]}')

		pos += 1

		if not tokens[pos].type == Tokens.NUMBER:
			raise Exception(f'Expected zone number but got: {tokens[pos]}')

		zone = int(tokens[pos].value)

		if is_first:
			# warm up
			start_power = zones.get_zone_range(zone).min
			end_power = zones.get_zone_range(zone).max
		elif is_last:
			# cool down
			start_power = zones.get_zone_range(zone).max
			end_power = zones.get_zone_range(zone).min
		else:
			start_power = zones.get_zone_range(zone).max
			end_power = zones.get_zone_range(zone).max


		return zwift.workouts.Steady(time, start_power, end_power)
	else:
		raise Exception(f'Expected either x or m after initial number token: {t}')
