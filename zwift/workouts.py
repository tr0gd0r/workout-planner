from dataclasses import dataclass

# times are in seconds

@dataclass
class Interval:
	reps: int
	on_time: int
	on_power: int
	off_time: int
	off_power: int

	def generate_zwift_xml(self):
		return f'<IntervalsT Repeat="{self.reps}" OnDuration="{self.on_time}" OffDuration="{self.off_time}" OnPower="{self.on_power}" OffPower="{self.off_power}"/>'

@dataclass
class Steady:
	time: int
	start_power: int
	end_power: int

	def generate_zwift_xml(self):
		if self.end_power == self.start_power:
			return f'<SteadyState Duration="{self.time}" Power="{self.start_power}"/>'
		elif self.end_power < self.start_power:
			return f'<Cooldown Duration="{self.time}" PowerLow="{self.end_power}" PowerHigh="{self.start_power}"/>'
		elif self.end_power > self.start_power:
			return f'<Warmup Duration="{self.time}" PowerLow="{self.start_power}" PowerHigh="{self.end_power}"/>'
