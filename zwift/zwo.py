import zwift.parser 

zwo = '''<workout_file>
	<author>anton</author>
	<name>{name}</name>
	<description>{description}</description>
	<sportType>bike</sportType>
	<workout>
		{workout_xml}
	</workout>
</workout_file>'''


def generate_file(workout_name, workout, zones, zwo_file, description=''):
	workout_steps = workout.split(',')
	xml_steps = []
	for i, step in enumerate(workout_steps):
		step = step.strip()
		rs = zwift.parser.RawStep(step) 
		tokens = zwift.parser.lex(rs)
		is_first = i == 0
		is_last = i == len(workout_steps)-1
		parsed_step = zwift.parser.parse(tokens, zones, is_first=is_first, is_last=is_last)
		xml_step = parsed_step.generate_zwift_xml()
		xml_steps.append(xml_step)
	zwo_file.write(zwo.format(name=workout_name, description=description, workout_xml='\n'.join(xml_steps)))