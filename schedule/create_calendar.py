import datetime
import json

from ics import Calendar, Event

def load_workouts(workouts_file):
	workouts = {}

	for line in workouts_file:
			workout = json.loads(line)
			workouts[workout['id']] = workout

	return workouts


def load_schedule(schedule_file):
	schedule = {}

	raw_schedule = json.load(schedule_file)

	for date, workouts in raw_schedule.items():
		if workouts:
			d = datetime.datetime.strptime(date, "%Y-%m-%d")
			schedule[d] = workouts

	return schedule


def create_description(day_workouts):
	description = ''

	for w in day_workouts:
		description += '{}: {}\n'.format(w['type'], w['duration'])

	description += '\n'

	for w in day_workouts:
		description += '{}: {}\n'.format(w['type'], w['id'] if w['type'] == 'bike' else '')
		description += '{}\n'.format(w['workout'])
		description += '{}\n'.format(w['zones'])
		description += '\n'

	return description


def create_workout_events(calendar, schedule, workouts):
	for date, workout_codes in schedule.items():
		todays_workouts = [workouts[code] for code in workout_codes]
		e = Event()
		e.name = 'Workout: {}'.format(
			' + '.join([w['type'] for w in todays_workouts]))
		e.begin = date
		e.description = create_description(todays_workouts)
		e.make_all_day()
		calendar.events.add(e)


def create_calendar(schedule_file, workouts_file, ics_out_file):
	s = load_schedule(schedule_file)
	w = load_workouts(workouts_file)
	c = Calendar()
	create_workout_events(c, s, w)
	ics_out_file.writelines(c)


if __name__ == '__main__':
	main()