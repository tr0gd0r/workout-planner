from datetime import timedelta
import json
import re

from bs4 import BeautifulSoup

SWIM = 'swim'
BIKE = 'bike'
RUN = 'run'

zone_regex = re.compile(r'.*(Zone\s\d).*')

with open('8020.html') as fp:
    soup = BeautifulSoup(fp, features="html.parser")

with open('zones.json') as f:
    zone_json = json.load(f)

for table in soup.find_all('tbody'):
    for row in table.find_all('tr'):
        cols = row.find_all('td')
        if len(cols) == 6:
            workout_type = SWIM
            code = row.find('td', class_='column-1').string
            total = row.find('td', class_='column-2').string
            description = row.find('td', class_='column-6').string
        else:
            link = row.find('td', class_='column-1').find('a')
            _workout_type =  str(link['href'].split('/')[3])
            workout_type = BIKE if _workout_type == 'bike' else RUN
            code = link.string
            total = row.find('td', class_='column-2').string
            description = row.find('td', class_='column-3').string

        if workout_type ==  RUN:
            if 'mi' in total:
                total_fmt = total
            else:
                total_fmt = str(timedelta(minutes=int(total)))[:-3]
        elif workout_type == BIKE:
            total_fmt = str(timedelta(minutes=float(total)))[:-3]
        elif workout_type == SWIM:
            total_fmt = '{}yd'.format(total)

        # parse description
        description = description.replace('Zone ', 'Z')
        description = description.replace(' minutes in', 'm')
        description = description.replace(' minutes', 'm')
        description = description.replace(' minute', 'm')
        description = description.replace(' seconds', 's')
        description = description.replace(') ', '), ')
        description = description.replace(' uphill', '')
        description = description.replace(' uphill or simulated', '')

        zones = re.findall(r'Z\d', description)
        zones = sorted([z for z in set(zones)])

        description_zones = []
        for z in zones:
            ranges = zone_json[workout_type][z]
            blah = '{} = {}'.format(z, ranges)
            description_zones.append(blah)

            
        row = {
            'id': code.lower(),
            'type': workout_type,
            'duration': total_fmt,
            'workout': description,
            'zones': description_zones, 
        }
        print(json.dumps(row))
        #print '{}^{}^{}'.format(
        #        code.lower(), description, description_zones)
        #print '{},{},{},{}'.format(
        #    code.lower(), total_fmt, description, description_zones
        #)

