import argparse
from collections import OrderedDict
from datetime import datetime, timedelta
import json

TIMEFMT = '%Y-%m-%d'

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--start', required=True)
	parser.add_argument('--end', required=True)
	args = parser.parse_args()

	start = datetime.strptime(args.start, TIMEFMT)
	end = datetime.strptime(args.end, TIMEFMT)

	output = {}
	incr = timedelta(days=1)
	current = start

	while current <= end:
		k = current.strftime(TIMEFMT)
		output[k] = []
		current += incr

	print(json.dumps(output, indent=4))



if __name__ == '__main__':
	main()